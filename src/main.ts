import "normalize.css";
import "./styles.scss";
import "reflect-metadata";

import "whatwg-fetch";
import { createApp } from "vue";
import App from "./app.component.vue";
import appStore from "./stores";

createApp(App).use(appStore).mount("#app");
