import { createStore, StoreOptions } from "vuex";
import { user } from "./user/";
import { IRootState } from "./types";

const store: StoreOptions<IRootState> = {
  state: {
    helloMessage: "Chater tech front #1",
  },
  modules: {
    user,
  },
};

export default createStore<IRootState>(store);
