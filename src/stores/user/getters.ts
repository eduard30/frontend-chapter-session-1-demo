import { GetterTree } from "vuex";
import { IRootState } from "../types";
import { IUserState } from "./types";

export interface Getters {
  username(state: IUserState): string;
};

export const getters: GetterTree<IUserState, IRootState> & Getters = {
  username: (state) => {
    return state.username;
  },
};
