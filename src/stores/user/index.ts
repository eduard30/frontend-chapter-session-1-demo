import { Module } from "vuex";
import { IRootState } from "../types";
import { IUserState } from "./types";
import { getters } from "./getters";

const state: IUserState = {
  username: "Oompa Loompa",
  email: "chocolate@factory.com",
  lastLogin: new Date(),
};

export const user: Module<IUserState, IRootState> = { state, getters };
