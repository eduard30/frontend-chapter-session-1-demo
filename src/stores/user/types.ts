export interface IUserState {
  username: string;
  email: string;
  lastLogin: Date;
}
