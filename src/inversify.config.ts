import { Container } from "inversify";
import { EmailValidationService } from "./lib/services/email-validation/email-validation.service";
import { OtherService } from "./lib/services/other-service/other.service";

const AppContainer = new Container({ defaultScope: "Singleton" });
const AppContainerSymbol = Symbol("AppContainer");

AppContainer.bind<EmailValidationService>(EmailValidationService)
  .toSelf()
  .inSingletonScope();
AppContainer.bind<OtherService>(OtherService).toSelf().inSingletonScope();

export { AppContainer, AppContainerSymbol };
