export interface IMailboxEmailCheckResponse {
  catch_all: string;
  did_you_mean: string;
  disposable: boolean;
  domain: string;
  email: string;
  format_valid: boolean;
  free: boolean;
  mx_found: boolean;
  role: boolean;
  score: number;
  smtp_check: boolean;
  user: string;
}
