import { HTTPVerbs } from "@/lib/enums/http-verbs.enum";
import { IMailboxEmailCheckResponse } from "@/lib/models/mailbox-email-check-response.model";
import { inject, injectable } from "inversify";
import { OtherService } from "../other-service/other.service";

@injectable()
export class EmailValidationService {
  /*
   * RegEx is purposely more permisive in order accomodate a wide range on characters
   * that could or could not be supported by SMTP servers, email provides, databases, etc.
   *
   * Please note that one of the only reliable ways to determinat that an email is truly
   * valid is to have an interaction with email in cause.
   *
   * Example:
   * Sending an activation link and waiting for the email owner to use the link in order
   * to confirm email is real and usable.
   */
  /**
   *
   */
  constructor(
    @inject(OtherService) private readonly otherService: OtherService // example of constructor injection
  ) {}

  private readonly emailValidationPattern: RegExp = new RegExp(
    /^(\S+@\S+\.\S+){1,320}$/
  );

  private fetchEmailsValidity(
    email: string
  ): Promise<IMailboxEmailCheckResponse> {
    return new Promise((resolve, reject) => {
      fetch(
        `${process.env.VUE_APP_MAILBOX_API_BASE_URL}?access_key=${process.env.VUE_APP_MAILBOX_LAYER_API_KEY}&email=${email}&smtp=1&format=1`,
        {
          method: HTTPVerbs.GET,
        }
      ).then(
        (response) => {
          response.json().then((val: IMailboxEmailCheckResponse) => {
            resolve(val);
          });
        },
        () => {
          reject(email + " could not be validated");
        }
      );
    });
  }

  public async validateEmails(emails: string[]): Promise<string[]> {
    const results: string[] = [];
    const promisePool: Promise<IMailboxEmailCheckResponse>[] = [];

    for (let i = 0; i < emails.length; i++) {
      if (!this.emailValidationPattern.test(emails[i].trim())) {
        results.push(emails[i].trim() + " is not a valid email");
        continue;
      }

      promisePool.push(this.fetchEmailsValidity(emails[i].trim()));
    }

    await Promise.allSettled(promisePool).then(
      (
        settledPromisePool: PromiseSettledResult<IMailboxEmailCheckResponse>[]
      ) => {
        settledPromisePool.forEach((promise) => {
          if (promise.status === "fulfilled") {
            if (!promise.value.format_valid) {
              results.push(promise.value.email + " is not a valid email");
            }
          } else {
            results.push(promise.reason);
          }
        });
      }
    );

    return results;
  }
}
