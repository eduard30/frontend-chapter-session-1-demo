import "whatwg-fetch";
import { IMailboxEmailCheckResponse } from "@/lib/models/mailbox-email-check-response.model";
import { EmailValidationService } from "./email-validation.service";
import { OtherService } from "../other-service/other.service";

describe("EmailValidationService", () => {
  const testService = new EmailValidationService(new OtherService());
  const mockInvalidResponse: IMailboxEmailCheckResponse = {
    catch_all: "",
    did_you_mean: "",
    disposable: false,
    domain: "",
    email: "invalid email",
    format_valid: false,
    free: false,
    mx_found: false,
    role: false,
    score: 0.2,
    smtp_check: false,
    user: "",
  };

  const mailBoxLayerMock = new Promise<Response>((resolve, reject) => {
    resolve({
      json: () => Promise.resolve(mockInvalidResponse as any),
    } as Response);
  });

  it("it should instantiate", () => {
    expect(testService).toBeTruthy();
  });

  describe("validateEmails", () => {
    it("should call fetch for each email address that passes local validation", () => {
      jest.spyOn(globalThis, "fetch");

      testService.validateEmails([
        "something@gmail.com",
        "my.email@g'mail",
        "ricky@ro'll.com",
      ]);

      expect(fetch).toHaveBeenCalledTimes(2);
    });

    it("should not call fetch if emails do not pass local validation", () => {
      jest.spyOn(globalThis, "fetch");

      testService.validateEmails(["a", "b", "c"]);

      expect(fetch).not.toBeCalled();
    });

    it("should return an array of validation results", () => {
      jest.spyOn(globalThis, "fetch").mockReturnValue(mailBoxLayerMock);

      testService
        .validateEmails(["my.email@g'mail", "ricky@roll.com"])
        .then((result: string[]) => {
          expect(result).toEqual([
            "my.email@g'mail is not a valid email",
            "invalid email is not a valid email",
          ]);
        });
    });

    it("return array of invalid emails and emails it could not validate", () => {
      jest.spyOn(globalThis, "fetch").mockReturnValue(Promise.reject());

      testService
        .validateEmails(["my.email@g'mail", "ricky@roll.com"])
        .then((result: string[]) => {
          expect(result).toEqual([
            "my.email@g'mail is not a valid email",
            "ricky@roll.com could not be validated",
          ]);
        });
    });

    afterEach(() => {
      jest.clearAllMocks();
    });
  });
});
