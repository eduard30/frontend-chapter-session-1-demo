import TooltipComponent from "./tooltip.component.vue";
import { shallowMount } from "@vue/test-utils";

describe("TooltipComponent", () => {
  const wrapper = shallowMount<TooltipComponent>(TooltipComponent, {});

  it("it should mount component", () => {
    expect(wrapper).toBeTruthy();
  });
});
