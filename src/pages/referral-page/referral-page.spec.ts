import { AppContainer, AppContainerSymbol } from "@/inversify.config";
import { shallowMount } from "@vue/test-utils";
import ReferralPageComponent from "./referral-page.component.vue";

describe("ReferralPageComponent", () => {
  const wrapper = shallowMount<ReferralPageComponent>(ReferralPageComponent, {
    global: {
      provide: {
        [AppContainerSymbol]: AppContainer,
      },
    },
  });

  it("it should mount component", () => {
    expect(wrapper).toBeTruthy();
  });

  describe("handleInviteEmailsInput", () => {
    it("should call clear timeout", () => {
      expect(wrapper.vm.handleInviteEmailsInput).toBeTruthy();
    });
  });
});
