import "reflect-metadata";
import { Container } from "inversify";

const AppContainer = new Container({ defaultScope: "Singleton" });
const AppContainerSymbol = Symbol("AppContainer");

export { AppContainer, AppContainerSymbol };
