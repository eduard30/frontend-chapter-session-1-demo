module.exports = {
  css: {
    loaderOptions: {
      sass: {
        prependData: `
                    @import "@/assets/styles/variables/_colors.scss";
                    @import "@/assets/styles/variables/_fonts.scss";
                `,
      },
    },
  },
};
