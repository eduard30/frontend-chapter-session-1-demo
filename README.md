# Frontend Chapter Session 1 Demo

## Creation

The project has been created using the `Vue CLI 4.5.12` with the following presets:
- Vue 3
- Typescript
- Babel
- SASS / SCSS (node-sass)
- ESLint
- Prettier
- Jest

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```

### Vue Config File
See [Configuration Reference](https://cli.vuejs.org/config/).

The project has been configured using the `vue.config.js` file in order to prepend SCSS variables which would make them available in all `.scss` file without imports.

### Environment

See [.env file](https://gitlab.com/eduard30/frontend-chapter-session-1-demo/-/blob/3ef18960cfe86212f2f6c4cbfe251cb79538948e/.env ".env file")

You can find 2 environment variables in the `.env` file which are loaded regardless of the mode in which you run the application (E.g: `production` or `development`).

Those variables hold the API key for the [Mailbox layer API](https://mailboxlayer.com/ "Mailbox layer API") and the API base url.

### Dependency Injection

The project makes use of [`inversify.js`](https://inversify.io/ "`inversify.js`") as a IoC Container.

Configuration can be found in ```inversify.config.ts```

### State Management

The project makes use of [`Vuex`](https://vuex.vuejs.org/ "`Vuex`") for state management.

You can find the initialisation code in `/src/main.ts` and all the other store files under `/src/stores`.

### Testing configuration

See [jest config file](https://github.com/EduardCarmocanu/luko/blob/89017d1b0c0c93d010c3125c34edf67023c590fa/jest.config.js)

Unit testing has been configured to look in all folders under `/src` in order to keep test files close to what is actually being tested in the file.

Example structure:

```.
├── ...
├── component-folder                       # wrapping folder
│   ├── example.component.vue     <----    # file that is tested
│   ├── example.spec.ts           <----    # test file
│   ├── example.styles.scss                # scss files
│   └── ...                                # etc.
```

Additionally testing has been configured to make use of the inversify test config file ```inversify-test.config.ts```

### Global Styles

The project makes use of [`normalize.css`](https://necolas.github.io/normalize.css/ "`normalize.css`") in order to maintain consistent styling and over different browsers / user devices

### Browser Compatibility

Besides the [default browser compatibility ](https://cli.vuejs.org/guide/browser-compatibility.html#browserslist "default browser compatibility ") packages brought in by the Vue CLI the project makes use of the following ones specific to our use case:
- whatwg-fetch
- clipboard-polyfill


------------

(▀̿Ĺ̯▀̿ ̿) ヾ(⌐■_■)ノ♪
